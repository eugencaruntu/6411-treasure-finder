;; ==============================================
;; Author: Eugen Caruntu
;; ID:     29077103
;; A treasure puzzle solver based on backtracking
;; ==============================================

;; Read the input file into a vector of strings (each line becomes a vector entry)
(defn read-file
  [filename]
  (-> (slurp filename)
      (clojure.string/split #"\s+")))

;; Create a board representation as a vector of vectors by splitting into characters
(def board
  (mapv (fn [l]
          (clojure.string/split l #""))
        (read-file "map.txt")))

;; The board dimensions
(def heigth (count board))
(def width  (count (first board)))

;; Helper to convert the board to a pretty string
(defn to-string
  [board]
  (let [string (apply str "\n" (clojure.string/join "\n" board) "\n")]
    (clojure.string/replace string #"[ \[\]\"]" "")))

;; Validate the map input and throw exceptions when inappropriate:
;;  1. the size of each list is the same across all row vectors
;;  2. only characters -, #, @ are permitted
(defn validate-board
  [board]
  (doseq [line board]
    (when-not (= width (count line))
      (throw (RuntimeException. (str "The size of this row: " (apply str line) " (" (count line) ") is not matching the size of first row (" width ")"))))
    (doseq [cell line]
      (when-not (= nil (re-find #"[^-#@]" cell))
        (throw (RuntimeException. (str "An incorrect character was detected (" cell ") on this row: " (apply str line)))))))) 

;; Apply the validation checks and stop the execution with the corresponding message if necessary
(try
  (validate-board board)
  (catch RuntimeException e
    (println "MAP INPUT ERROR:" (.getMessage e))
    (println "This is the faulty board:" (to-string board))
    (System/exit 0)))

;; Display the board after validation
(println "This is my challenge:\n" (to-string board))

;; Test current cell value for goal matching
(defn goal?
  [board [y x]]
  (= "@" (get-in board [y x])))

;; Mark the position as visited
(defn mark-visited
  [board [y x]]
  (assoc-in board [y x] "+"))

;; Mark the position as a dead-end
(defn mark-dead
  [board [y x]]
  (assoc-in board [y x] "!"))

;; Test if the position is valid:
;;  1. must be within the board
;;  2. must be either permitted "-" or the goal "@"
(defn allowed?
  [board [y x]]
  (and
   (<= 0 y)
   (< y heigth)
   (<= 0 x)
   (< x width)
   (or
    (= "-" (get-in board [y x]))
    (= "@" (get-in board [y x])))))

;; Recursively look for the goal using backtracking, and return a tuple made of an updated board and a found flag:
;; 1. First, validate the position
;; 2. Then, if position is the goal, return the board and found
;; 3. Otherwise, try moving successively in either one of the 4 directions until a goal is found or dead-end
;;    - each time, pass the board from one call to the next
;;    - start by marking the position as visited "+"
;;    - when dead-end, backtrack and mark the position as "!"
;; 4. If the position is invalid, at the end, return the board and failure
(defn find-goal
  [board [y x] found]
  (if (allowed? board [y x])
    (if (goal? board [y x])
      [board true]   ; return with success
      (let [result (find-goal (mark-visited board [y x]) [y (+ x 1)] false)] ; go right
        (if (second result)
          result
          (let [result (find-goal (first result) [(+ y 1) x] false)]         ; go down
            (if (second result)
              result
              (let [result (find-goal (first result) [y (- x 1)] false)]     ; go left
                (if (second result)
                  result
                  (let [result (find-goal (first result) [(- y 1) x] false)] ; go up
                    (if (second result)
                      result
                      [(mark-dead (first result) [y x]) false])))))))))      ; return with dead-end
    [board false]))   ; return with failure

;; A driver function to launch the search and print the final board based on the result
(defn solve
  [board [y x]]
  (let [solution (find-goal board [y x] false)]
    (if (second solution)
      (println "Woo hoo, I found the treasure :-)\n" (to-string (first solution)))
      (println "Uh oh, I could not find the treasure :-(\n" (to-string (first solution))))))

;; Start the search from the top left corner
(solve board [0 0])